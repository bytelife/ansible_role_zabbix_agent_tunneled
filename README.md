zabbix_agent_tunneled
=========

Used to deploy Zabbix Agents in active mode, which are connected to Serevr over SSH tunnels.
Typical usecase: cloud servers without VPN to central Zabbix server/proxy

Requirements
------------

- Tested with Ansible 2.9
- Zabbix proxy has to be provisioned along with SSH tunnel server, alog with temporary password (playbook deploy_proxy.yml from repo zabbix_proxy).
- Should have access to SSh tunnel server public hostkey, location determined by variable `sshkeys_dir`.


Variables
--------------


- Hostvar, from inventory. - port, where agent will listen for Server connection, unique in each server 

```
zabbix_port: 10070
```


- role vars:

```
# -------- update role level, set up:
# TEMP, used during agent deploys. Reset, if all reployed (needed for agent hosts pub key uploads)
ssh_pass: changeit

# this will be overriden by every customer to allocated port
ssh_tunnel_port: 21000

# where to forward that connection for this customer (cant use IP as dynamic IP for container)
zabbix_proxy_name: zbx_prx_clientname

# --------- from  defaults/main.yml, can be overriden in role use
# tunnelserver pub key dir, assumes zaabix-proxys are deployed from same computer as this,
sshkeys_dir: ../zabbix_proxy/ssh_hostkeys

# proxy (or NAT device, if internet), defaults public aadresses, bls fw here
ssh_tunnel_ip: 81.20.146.53

# will keep default most likely
zabbix_proxy_port: 10051

```

Dependencies
------------

- Role: community.zabbix.zabbix_agent

Install role
------------

- Add following to requirements.yml:
```
- name: zabbix-agent-tunneled
  src: git+https://bitbucket.org/bytelife/ansible_role_zabbix_agent_tunneled
```

- run 
```
ansible-galaxy role install -r requirements.yml
```

Example Playbook
----------------

```
- hosts: zabbix_agents
  become: true
  roles:
    - role: zabbix-agent-tunneled
      vars:
#        below are usually coming from inventory
#        zabbix_proxy_name: zbx_prx_client01
#        ssh_tunnel_port: 21001
#        ssh_pass: ChangeME
```

Example inventory
-----------------
usually in inventory/host-customerX.conf
each host should have unique listening port:

```
[zabbix_agents]
# autossh_port peab olema 2 kaupa
logstack-docker01 zabbix_port=10071 autossh_port=22200
logstack-docker02 zabbix_port=10072 autossh_port=22202

[zabbix_agents:vars]
zabbix_proxy_name=zbx_prx_bls  # where to forward that connection (cant use IP as dynamic IP for container)
ssh_tunnel_ip=94.154.144.27     # BLS fw external addr
ssh_tunnel_port=21007          # public internet port
ssh_pass=
```

License
-------

GPL 3.0

Author Information
------------------
kalev @ bytelife.ee
